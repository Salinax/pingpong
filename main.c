#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Pantalla.h"
#include "Pelota.h"
#include "Vector.h"
#include "Jugador.h"

#define wPantalla 600
#define hPantalla 440
#define wJugador wPantalla/25
#define hJugador hPantalla/4

int main(int argc, char **argv)
{
    int terminado = 0;
    int resultado = 0;
    srand(time(0));

    Pantalla_Crea("PingPong",wPantalla+40,hPantalla+40);
    Pelota pelota = PelotaCrea(10,VectorCrea(3,3),VectorCrea(wPantalla/2,hPantalla/2));
    Jugador jugador1 = JugadorCrea(VectorCrea(6,6),VectorCrea(0,hPantalla/2),hJugador,wJugador,1,SDL_SCANCODE_W,SDL_SCANCODE_S);
    Jugador jugador2 = JugadorCrea(VectorCrea(6,6),VectorCrea(wPantalla-wJugador,hPantalla/2),hJugador,wJugador,1,SDL_SCANCODE_UP,SDL_SCANCODE_DOWN);
    int contL = 0;
    int contR = 0;
    char contLStr[10];
    char contRStr[10];
    while (Pantalla_Activa() && !terminado)
    {

        if (Pantalla_TeclaPulsada(SDL_SCANCODE_ESCAPE))
        {
            terminado = 1;
        }
        JugadorMueve(&jugador1,PelotaGetPosicion(pelota));
        JugadorMueve(&jugador2,PelotaGetPosicion(pelota));
        resultado = PelotaMueve(&pelota,jugador1,jugador2);
        if(resultado > 0) PelotaReset(&pelota,VectorCrea(3,3),VectorCrea(wPantalla/2,hPantalla/2));

        Pantalla_ColorRelleno(0,0,0,255);
        Pantalla_DibujaRectangulo(0,0,wPantalla+40,hPantalla+40);
        Pantalla_ColorRelleno(100,0,0,255);
        Pantalla_DibujaRectangulo(0,hPantalla,wPantalla+40,40);
        Pantalla_DibujaRectangulo(0,0,20,hPantalla);
        Pantalla_DibujaRectangulo(wPantalla+20,0,20,hPantalla);
        Pantalla_DibujaRectangulo(0,0,wPantalla+40,20);
        Pantalla_ColorRelleno(0,0,0,255);
        Pantalla_DibujaRectangulo(wPantalla/4,hPantalla+10,25,20);
        Pantalla_DibujaRectangulo(3*(wPantalla/4),hPantalla+10,25,20);
        Pantalla_ColorTrazo(255,255,255,255);
        if (resultado == 2) contR++;
        else if (resultado == 1) contL++;
        sprintf(contLStr, "%d", contL);
        sprintf(contRStr, "%d", contR);
        Pantalla_DibujaTexto(contLStr,(wPantalla/4)+8,(hPantalla+10)+3);
        Pantalla_DibujaTexto(contRStr,(3*(wPantalla/4))+8,(hPantalla+10)+3);

        PelotaDibuja(pelota);
        JugadorDibuja(jugador1);
        JugadorDibuja(jugador2);
        Pantalla_Actualiza();
        Pantalla_Espera(5);
    }
    Pantalla_Libera();

    return 0;
}

