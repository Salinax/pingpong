#ifndef __Vector_H__
#define __Vector_H__

typedef struct stVector
{
    float x;
    float y;
} Vector;

Vector VectorCrea(float x, float y);
float VectorGetY(Vector v);

#endif
