#include "Pelota.h"
#include "Pantalla.h"

#define W_SCREEN 600
#define H_SCREEN 440

Pelota PelotaCrea(int radio, Vector velocidad, Vector posicion)
{
    Pelota p;
    p.radio = radio;
    p.velocidad = velocidad;
    p.posicion = posicion;
    return p;
}

void PelotaDibuja(Pelota p)
{
    Pantalla_ColorRelleno(255,255,100,255);
    Pantalla_ColorTrazo(255,255,100,255);
    Pantalla_DibujaCirculo(p.posicion.x,p.posicion.y,p.radio);
    Pantalla_ColorTrazo(0,0,0,0);
}

void PelotaReset(Pelota *p,Vector velocidad,Vector posicion){
    p->posicion = posicion;
    p->velocidad = velocidad;
}

int PelotaMueve(Pelota *p, Jugador j1, Jugador j2) //Devuelve un 1 si ha tocado en el lado derecho y un 2 si ha tocado en el lado izquierdo
{
    //Colisiones con las paredes
    if(p->posicion.x + p->velocidad.x > W_SCREEN){
        return 1;
    }
    if(p->posicion.x + p->velocidad.x < 0){
        return 2;
    }
    if(p->posicion.y + p->velocidad.y > H_SCREEN){
        p->velocidad.y*=-1;
    }
    if(p->posicion.y + p->velocidad.y < 0){
        p->velocidad.y*=-1;
    }

    //Colisiones con jugador 1
    if((p->posicion.x + p->velocidad.x <= j1.posicion.x+j1.weigth)
       &&(p->posicion.y + p->velocidad.y <= j1.posicion.y+j1.heigth)
       &&(p->posicion.y + p->velocidad.y >= j1.posicion.y)){
        p->velocidad.x*=-1;
        p->velocidad.x*=1.1;
        p->velocidad.y*=1.1;
    }
    //Colisiones con jugador 2
    if((p->posicion.x + p->velocidad.x >= j2.posicion.x)
       &&(p->posicion.y + p->velocidad.y <= j2.posicion.y+j2.heigth)
       &&(p->posicion.y + p->velocidad.y >= j2.posicion.y)){
        p->velocidad.x*=-1;
        p->velocidad.x*=1.1;
        p->velocidad.y*=1.1;
    }


    p->posicion.x += p->velocidad.x;
    p->posicion.y += p->velocidad.y;

    return 0;
}

Vector PelotaGetPosicion(Pelota p){
    return p.posicion;
}
