#include <stdlib.h>

#include "Jugador.h"

#define W_SCREEN 600
#define H_SCREEN 440

Jugador JugadorCrea(Vector velocidad, Vector posicion, int heigth, int weigth, int ia,SDL_Scancode arriba, SDL_Scancode abajo)
{
    Jugador j;
    j.ia = ia;
    j.velocidad = velocidad;
    j.posicion = posicion;
    j.heigth=heigth;
    j.weigth=weigth;
    j.arriba = arriba;
    j.abajo = abajo;
    j.r = (rand()%128)+127;
    j.g = (rand()%128)+127;
    j.b = (rand()%128)+127;
    return j;
}

void JugadorDibuja(Jugador j)
{
    Pantalla_ColorRelleno(j.r,j.b,j.g,255);
    Pantalla_ColorTrazo(j.r,j.b,j.g,255);
    Pantalla_DibujaRectangulo(j.posicion.x+20,j.posicion.y,j.weigth,j.heigth);
    Pantalla_ColorTrazo(0,0,0,0);
}

void JugadorMueve(Jugador *j, Vector v)
{
    if (j->ia){
        if(j->posicion.y+j->heigth/2 > VectorGetY(v)){
            if(j->posicion.y - j->velocidad.y >= 0+20)
                j->posicion.y-=j->velocidad.y;
        } else if (j->posicion.y+j->heigth/2 < VectorGetY(v)){
            if(j->posicion.y + j->velocidad.y <= H_SCREEN-j->heigth)
                j->posicion.y+=j->velocidad.y;
        }
    } else {
        if(Pantalla_TeclaPulsada(j->arriba)){
            if(j->posicion.y - j->velocidad.y >= 0+20)
                j->posicion.y-=j->velocidad.y;
        }
        if(Pantalla_TeclaPulsada(j->abajo)){
            if(j->posicion.y + j->velocidad.y <= H_SCREEN-j->heigth)
                j->posicion.y+=j->velocidad.y;
        }
    }



}
