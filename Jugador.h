#ifndef __Jugador_H__
#define __Jugador_H__

#include "Vector.h"
#include "Pantalla.h"


typedef struct stJugador
{
    int heigth;
    int weigth;
    Vector posicion;
    Vector velocidad;
    int ia;
    int r,g,b;
    SDL_Scancode arriba;
    SDL_Scancode abajo;
} Jugador;

Jugador JugadorCrea(Vector velocidad, Vector posicion, int heigth, int weigth, int ia,SDL_Scancode arriba, SDL_Scancode abajo);
void JugadorDibuja(Jugador j);
void JugadorMueve(Jugador *j, Vector v);

#endif



