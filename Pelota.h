#ifndef __Pelota_H__
#define __Pelota_H__

#include "Vector.h"
#include "Jugador.h"

typedef struct stPelota
{
    int radio;
    Vector posicion;
    Vector velocidad;
} Pelota;

Pelota PelotaCrea(int radio, Vector velocidad, Vector posicion);
void PelotaDibuja(Pelota p);
void PelotaReset(Pelota *p,Vector velocidad,Vector posicion);
int PelotaMueve(Pelota *p, Jugador j1, Jugador j2);
Vector PelotaGetPosicion(Pelota p);

#endif



